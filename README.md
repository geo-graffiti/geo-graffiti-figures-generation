# ChatGPT Polygon paths Generation and store them in PostgreSQL

## Table of Contents
- [Context](#introduction)
- [Prerequisites](#prerequisites)

## Context
Here we use ChatGPT to generate figures and store them in a PostgreSQL database. 

## Prerequisites
Before you begin, make sure you have the necessary libraries. See requirement.txt

