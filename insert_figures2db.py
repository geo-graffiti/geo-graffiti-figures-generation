import psycopg2
import json
import os
# Database connection parameters
# db_host = "34.71.98.8"
# db_port = "5432"  # Default PostgreSQL port
# db_name = "postgres"
# db_user = "postgres"
# db_password = """Mi1)SFDPRz,8^S":"""
# db_logs_file = 'db_logs.txt'

db_host = os.environ.get("db_host")
db_port = os.environ.get("db_port")
db_name = os.environ.get("db_name")
db_user = os.environ.get("db_user")
db_password = os.environ.get("db_password")

db_logs_file = 'db_logs.txt' # to log inserts



def insert_figures2db(data, city_name):
    # Connect to the PostgreSQL database
    conn = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_password,
        host=db_host,
    )

    # Create a cursor
    cur = conn.cursor()

    with open(db_logs_file, 'a') as file:
        for path in data["paths"]:
            perimeter = path["perimeter"]
            number_of_vertices = len(path["vertices"])
            vertices = json.dumps({'vertices': path["vertices"]})
            name = path["path_name"]
            description = path["description"]
            city = city_name
            score = number_of_vertices*perimeter

            # Define the SQL query with placeholders for parameters
            sql_query = """INSERT INTO figure (perimeter, number_of_vertices, score, vertices, name, description, city)
                           VALUES (%s, %s, %s, %s::jsonb, %s, %s, %s)"""

            insert_command = cur.mogrify(sql_query, (
            perimeter, number_of_vertices, score, json.dumps(vertices), name, description, city)).decode('utf-8')
            print(insert_command)
            # Execute the SQL query with parameters
            cur.execute(sql_query,
                        (perimeter, number_of_vertices, score, json.dumps(vertices), name, description, city))

            # Append the insert command to the file
            file.write(insert_command + '\n')

    # Commit the changes and close the cursor and connection
    conn.commit()
    cur.close()
    conn.close()


if __name__ == '__main__':
    with open('quadrilateral_v2_Helsinki.json', 'r') as json_file:
        # Load the JSON data from the file into a Python data structure
        json_data = json.load(json_file)
    city_name = 'Helsinki'
    # print(type(json_data))
    # print(json_data['paths'])
    insert_figures2db(json_data, city_name)
